
#ifndef ROBOT_SPECS_H
#define ROBOT_SPECS_H
 
#define encoder_pulse   3.15 // counts per revolution of the encoders in each channel
#define gear_ratio      19  // transformation ratio of the gearmotor with encoder
#define wheel_diameter  0.06   //diameter in m
#define wheel_width     0.06   // width or thickness of the wheels in m
#define track_width     0.3   // distance between the wheels in m
#define MAX_RPM         40 // maximum speed of the geared motors without load apparently
#define pi              3.1415926  
#define two_pi          6.2831853
#endif
