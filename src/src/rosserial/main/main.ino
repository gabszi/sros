
//ROS headers                                                                                         
#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
#endif
#include <ros.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Twist.h>
#include <ros/time.h>
#include "robot_specs.h"

//Motor Shield headers
#include "DualVNH5019MotorShield.h"                                                                  
DualVNH5019MotorShield md;                                                                      

                                                                                                      //VNH5019 initialize                                                                                                   
#define encodPinA1      18     // first encoder A output                                      
#define encodPinB1      34     // first encoder B output
#define encodPinA2      19     // second encoder A output
#define encodPinB2      35     // second encoder B output
#define LOOPTIME        100    // PID loop time(ms)
#define SMOOTH      10         //??

#define setmotorspeed

#define sign(x) (x > 0) - (x < 0)                                                                    

unsigned long lastMilli = 0;       // loop timing                                                    
unsigned long lastMilliPub = 0;
double rpm_req1 = 0;
double rpm_req2 = 0;
double rpm_act1 = 0;
double rpm_act2 = 0;
double rpm_req1_smoothed = 0;
double rpm_req2_smoothed = 0;                                                                        

int PWM_val1 = 0;                                                                                    
int PWM_val2 = 0;
volatile long count1 = 0;          // rev counter
volatile long count2 = 0;
long countAnt1 = 0;
long countAnt2 = 0;
float Kp =   0.05;     //0.5
float Kd =   0; //0.3
float Ki =   0; //0.01
double vel_1 =0;
double vel_2 =0;
ros::NodeHandle nh;                                                                          

void handle_cmd( const geometry_msgs::Twist& cmd_msg) {                                           
  double x = cmd_msg.linear.x;
  double z = cmd_msg.angular.z;
  if (z == 0) {     // go straight
    // convert m/s to rpm
    rpm_req1 = x*60/(pi*wheel_diameter);
    rpm_req2 = rpm_req1;
  }
  else if (x == 0) {
    // convert rad/s to rpm
    rpm_req2 = z*track_width*60/(wheel_diameter*pi*2);
    rpm_req1 = -rpm_req2;
  }
  else {
    rpm_req1 = x*60/(pi*wheel_diameter)-z*track_width*60/(wheel_diameter*pi*2);
    rpm_req2 = x*60/(pi*wheel_diameter)+z*track_width*60/(wheel_diameter*pi*2);
    
  }
}                                                         

ros::Subscriber<geometry_msgs::Twist> sub("cmd_vel", handle_cmd);                             
geometry_msgs::Vector3Stamped rpm_msg;
ros::Publisher rpm_pub("rpm", &rpm_msg);
ros::Time current_time;
ros::Time last_time;                                                                                

void setup() {                                                                                     

 Serial.begin(57600);                                                                      
 Serial.println("Dual VNH5019 Motor Shield");
 md.init();                                                                                          
 count1 = 0;
 count2 = 0;
 countAnt1 = 0;
 countAnt2 = 0;
 rpm_req1 = 0;
 rpm_req2 = 0;
 rpm_act1 = 0;
 rpm_act2 = 0;
 vel_1 =0;
 vel_2=0;
 PWM_val1 = 0;
 PWM_val2 = 0;
 nh.initNode();
 nh.getHardware()->setBaud(57600);
 nh.subscribe(sub);
 nh.advertise(rpm_pub);                                                                             
  
 pinMode(encodPinA1, INPUT);                                                                         
 pinMode(encodPinB1, INPUT); 
 digitalWrite(encodPinA1, HIGH);                // turn on pullup resistor
 digitalWrite(encodPinB1, HIGH);
                                                            //use of interrupt 3 on mega board (pin 18.19)

 pinMode(encodPinA2, INPUT); 
 pinMode(encodPinB2, INPUT); 
 digitalWrite(encodPinA2, HIGH);                // turn on pullup resistor
 digitalWrite(encodPinB2, HIGH);
 attachInterrupt(digitalPinToInterrupt(encodPinA2),encoder2, RISING);
 attachInterrupt(digitalPinToInterrupt(encodPinA1),encoder1, RISING);//use of interrupt 5 on mega board (pin 18)
                                                                                                    
 
 md.setM1Speed(0);                                                                              
 md.setM2Speed(0);                                                                                   

}                                                                                                    

void loop() {                                                                                        
  nh.spinOnce();
  unsigned long time = millis();
  if(time-lastMilli>= LOOPTIME)   {      // enter tmed loop                                          
    getMotorData(time-lastMilli);
    PWM_val1 = updatePid(1, PWM_val1, rpm_req1, rpm_act1);
    PWM_val2 = updatePid(2, PWM_val2, rpm_req2, rpm_act2);

if (rpm_req1==0)
 md.setM1Speed(0);
else
 //md.setM1Speed(rpm_req1*6.375);
 md.setM1Speed( PWM_val1);

if (rpm_req2==0)
 md.setM2Speed(0);
else
 //md.setM2Speed(rpm_req2*6.375);
 md.setM2Speed(PWM_val2);

    
    publishRPM(time-lastMilli);                                                                      
    lastMilli = time;
  }
  if(time-lastMilliPub >= LOOPTIME) {
  //  publishRPM(time-lastMilliPub);
    lastMilliPub = time;
  }
}                                                                                                    

void getMotorData(unsigned long time)  {                                                            
 rpm_act1 = (double((count1-countAnt1)*60*1000)/double(time*encoder_pulse*gear_ratio)) ;
 rpm_act2 = (double((count2-countAnt2)*60*1000)/double(time*encoder_pulse*gear_ratio)) ;


 //convert rpm to ms 
  vel_1 = rpm_act1 *pi*wheel_diameter/60;
  //Serial.println(vel_1);
  vel_2 =rpm_act2 *pi*wheel_diameter/60;
 countAnt1 = count1;
 countAnt2 = count2;
 //converting ticks to rpm  = pulse / ticks* gear ratio we got rpm
 //convertt rpm to seconds = rpm/60 
 //here we are going to find the speed in milli seconds
 
}                                                                                                

int updatePid(int id, int command, double targetValue, double currentValue) {                        
  double pidTerm = 0;                            // PID correction
  double error = 0;
  double new_pwm = 0;
  double new_cmd = 0;
  static double last_error1 = 0;
  static double last_error2 = 0;
  static double int_error1 = 0;
  static double int_error2 = 0;
  
  error = targetValue-currentValue;
  if (id == 1) {
    int_error1 += error;
    pidTerm = Kp*error + Kd*(error-last_error1) + Ki*int_error1;
    last_error1 = error;
  }
  else {
    int_error2 += error;
    pidTerm = Kp*error + Kd*(error-last_error2) + Ki*int_error2;
    last_error2 = error;
  }
  new_pwm = constrain(double(command)*MAX_RPM/255 + pidTerm, -MAX_RPM, MAX_RPM);
  new_cmd = 255*new_pwm/MAX_RPM;
  return int(new_cmd);
}                                                                                                   

void publishRPM(unsigned long time) {                                                               
  rpm_msg.header.stamp = nh.now();
rpm_msg.vector.x = vel_1;
  //rpm_msg.vector.x = rpm_req1*6.375;
 rpm_msg.vector.y = vel_2;
  //rpm_msg.vector.y =rpm_req2*6.375;
  rpm_msg.vector.z =PWM_val2;
  rpm_pub.publish(&rpm_msg);
  nh.spinOnce();
}                                                                                                    
                

void encoder1() {                                                                                    
  if (digitalRead(encodPinA1) == digitalRead(encodPinB1)) count1--;
  else count1++;
  
}
void encoder2() {
  if (digitalRead(encodPinA2) == digitalRead(encodPinB2)) count2++;
  else count2--;
}            
