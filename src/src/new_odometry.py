#!/usr/bin/env python

import math
from math import sin, cos, pi
from std_msgs.msg  import Float64
import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from sensor_msgs.msg import Imu


rospy.init_node('odometry_publisher')

odom_pub = rospy.Publisher("/odom", Odometry, queue_size=50)


odom_broadcaster = tf.TransformBroadcaster()

vel_x = 0
vel_y = 0
vel_th = 0
x = 0.0
y = 0.0
th = 0.0
current_theta = 0.0
last_theta = 0.0 




vel_x_left = 0.0
vel_x_right =0.0
vel_x_robot = 0.0
right_vel = 0.0
left_vel  = 0.0
left_vel_wheel = 0.0 
right_vel_wheel =0.0

WheelSeparation_distance = 0.24
LIMIT_X_VEL = 0.6
left_wheel_speed = 0.0
right_wheel_speed = 0.0
pid_right_vel_effort = 0.0
pid_left_vel_effort = 0.0



# function for definging odometry from the wheel encoder
def rps_to_ms(data):   #function for converting rotation per seconds to m/s
       global left_wheel_speed
       left_wheel_speed = data.vector.y
       global right_wheel_speed
       right_wheel_speed = data.vector.x
       #print(left_wheel_speed);
       
       

       global vel_x_robot
       vel_x_robot = (right_wheel_speed+left_wheel_speed)/2
       #print(vel_x_robot)	

current_time = rospy.Time.now()
last_time = rospy.Time.now()
r = rospy.Rate(30)





while not rospy.is_shutdown():
    current_time = rospy.Time.now()
  
    rospy.Subscriber("/rpm",Vector3Stamped,rps_to_ms,queue_size=1)

    


    #here we have to find the speed of the robot from the encoder
    # compute odometry in a typical way given the velocities of the robot
    dt = (current_time - last_time).to_sec()
    vx = vel_x_robot  #fake value
    #print(vel_x_robot)
    vy = 0

    vth =((left_wheel_speed-right_wheel_speed)/WheelSeparation_distance)
 

    delta_x = (vx * cos(th) - vy * sin(th)) * dt
    delta_y = (vx * sin(th) + vy * cos(th)) * dt
    delta_th = vth * dt
    global x
    global y
    global th
    x += delta_x
    y += delta_y
    th += delta_th


    ## here we are defining the wheel velocity to be published as tf
    wheels_tf = Twist()
   
    #print(pid_right_wheel_vel)

    # since all odometry is 6DOF we'll need a quaternion created from yaw
    odom_quat = tf.transformations.quaternion_from_euler(0, 0, th)

    # first, we'll publish the transform over tf
    odom_broadcaster.sendTransform(
        (x,y, 0.0),
        odom_quat,
        current_time,
        "base_link",
        "odom"
 )

    # next, we'll publish the odometry message over ROS
    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"

    # set the position
    odom.pose.pose = Pose(Point(x, y, 0), Quaternion(*odom_quat))
    odom.pose.covariance  =  [0.01, 0,    0,    0,    0,    0,    
                              0,    0.01, 0,    0,    0,    0,    
                              0,    0,    0.01, 0,    0,    0,    
                              0,    0,    0,    0.01, 0,    0,  
                              0,    0,    0,    0,    0.01, 0,   
                              0,    0,    0,    0,    0,    0.01]


    # set the velocity
    odom.child_frame_id = "base_link"
    odom.twist.twist = Twist(Vector3(vy, vy, 0), Vector3(0, 0, vth)) 
    odom_pub.publish(odom)

    last_time = current_time
    r.sleep()
