﻿# SROS

An educational mobile robot platform and ROS packages.

## Description

The main objective of the project is to more thoroughly study the capacity of the Robot Operating System (ROS) and implement autonomous navigation solution to an Arduino based mobile robot. The work consists of cost-efficient mobile robot design and 2D goal navigation on a predefined map that requires carrying out mapping and localization processes.

## Prerequisites

SolidWorks 2015 or later must be installed on the system, for the model.
ROS for the ROS packages


## Authors

* **Sergei Kuzin** - sergeiokuzin@gmail.com

## License

This project is the sole ownership of [UiT The Arctic University of Norway](uit.no).